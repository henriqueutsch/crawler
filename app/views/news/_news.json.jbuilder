json.extract! news, :id, :source, :url, :title, :subtitle, :publish_data, :collection_date, :body_text, :created_at, :updated_at
json.url news_url(news, format: :json)
