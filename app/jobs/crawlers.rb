class Crawlers
  def crawler_turismo(url, range, npage)
    total_pages = document.css('ul.listingBar li')[-2].text.strip
    (0..total_pages).map do |page|
      document = Nokogiri::HTML(open("#{url}?b_start:int=#{page*npage}"))
      document.css('div.conteudo').each do |item|
        data = item.css('span.data')[0].text
        request = {
          source: url,
          url: item.css('h2.titulo a')[0].attributes['href'].value.gsub(' ',''),
          title: item.css('h2.titulo').text.strip,
          subtitle: item.css('span.descricao').text.gsub(data, '').strip,
          publish_data: Date.parse(data.strip.gsub(' -', '')),
          collection_date: Date.today,
          body_text: ''
        }
        document = Nokogiri::HTML(open(request[:url]))
        request[:body_text] = document.css('div#content-core div div p')[0].text
        ap request
      end
    end
  end
end
