require 'webdrivers'
require 'open-uri'
require 'nokogiri'
require 'watir'
require 'headless'
require 'awesome_print'
require 'net/http'
require 'date'
require_relative "Validate"
require_relative "Crawlers"

def valid_url(url)
  res = Net::HTTP.get_response(URI.parse(url.to_s))
  return res.code == '200' ? true : false
end

def valid_args(args)
  valid_args = %w[all_news last_month last_n_months last_week today check_update].any? {|arg| args.include?(arg) }
end

def second_arg_is_method(args)
  valid_args(args[1])
end

def valid_number_of_args(args)
  args.count > 3 ? false : true
end

def valid_date(args)
  begin
    valid_begin_date_type = Date.parse(args[1]).instance_of?(Date)
    valid_end_date_type = Date.parse(args[2]).instance_of?(Date)
    valid_range_date = (Date.parse(args[1]) <= Date.parse(args[2]))
    return valid_begin_date_type == true && valid_end_date_type == true && valid_range_date == true ? true : false
  rescue e => error
    puts "Crawler Valid Date: Rescue #{e}"
    return false
  end
end

def convert_date_range(args)
  args[2] = 1 if args[2].nil?
  filters = {
    last_month: [Date.today.prev_month(1), Date.today],
    last_n_months: [Date.today.prev_month(args[2]), Date.today],
    last_week: [Date.today.prev_day(7), Date.today],
    last_n_weeks: [Date.today.prev_day(args[2]*7), Date.today],
    today: [Date.today, Date.today]
  }
  return filters[args[1]]
end

def run_method(args)
  switch(args)
end


def crawler_turismo(url, range, npage)
  total_pages = document.css('ul.listingBar li')[-2].text.strip
  (0..total_pages).map do |page|
    document = Nokogiri::HTML(open("#{url}?b_start:int=#{page*npage}"))
    document.css('div.conteudo').each do |item|
      data = item.css('span.data')[0].text
      request = {
        source: url,
        url: item.css('h2.titulo a')[0].attributes['href'].value.gsub(' ',''),
        title: item.css('h2.titulo').text.strip,
        subtitle: item.css('span.descricao').text.gsub(data, '').strip,
        publish_data: Date.parse(data.strip.gsub(' -', '')),
        collection_date: Date.today,
        body_text: ''
      }
      document = Nokogiri::HTML(open(request[:url]))
      request[:body_text] = document.css('div#content-core div div p')[0].text
      ap request
    end
  end
end

def run_crawler(*args)
  if valid_url(args.first) == true && valid_args(args) == true && valid_number_of_args(args)
    if second_arg_is_method(args) == true
      run_method(args[0],convert_date_range(args))
    else
      if valid_date(args) == true
        puts
      end
      # puts valid_date(args)
      # p convert_date_range(args)
      puts crawler_turismo(args[0],range,12)

    end


  end
end

# run_crawler('https://www.gov.br/turismo/pt-br/assuntos/noticias' , '08/06/2021', '09/06/2021')
# run_crawler('https://www.gov.br/turismo/pt-br/assuntos/noticias' , :last_n_months, 3)
run_crawler('https://www.gov.br/turismo/pt-br/assuntos/noticias', :last_n_months, 3)

# ap valid_url('https://www.gov.br/turismo/pt-br/assuntos/noticias')
# ap valid_url('http://mds.gov.br/area-de-imprensa/noticias')
