require 'open-uri'

class CrawlerpageJob < ApplicationJob
  queue_as :default

  def perform(*args)
    get_mds(args.first)
  end

  def get_mds(url)
    document = Nokogiri::HTML(open(url))
    document.css('div.tile-collective-nitf-content').each do |item|
      News.create(
        source: url,
        url: item.css('div.tileContent a')[0].attributes["href"].value.gsub(' ',''),
        title: item.css('h2.tileHeadline').text.strip,
        subtitle: item.css('span.subtitle').text,
        publish_data: item.css('span.summary-view-icon')[0].text.strip,
        collection_date: Date.today,
        body_text: item.css('span.description').text
      )
    end
  end
end
