require 'date'
require 'net/http'

class Validate
  def valid_url(url)
    res = Net::HTTP.get_response(URI.parse(url.to_s))
    return res.code == '200' ? true : false
  end

  def valid_args(args)
    %w[all_news last_month last_n_months last_week today check_update].any? {|arg| args.include?(arg) }
  end

  def second_arg_is_method(args)
    valid_args(args[1])
  end

  def valid_number_of_args(args)
    args.count > 3 ? false : true
  end

  def valid_date(args)
    begin
      valid_begin_date_type = Date.parse(args[1]).instance_of?(Date)
      valid_end_date_type = Date.parse(args[2]).instance_of?(Date)
      valid_range_date = (Date.parse(args[1]) <= Date.parse(args[2]))
      return valid_begin_date_type == true && valid_end_date_type == true && valid_range_date == true ? true : false
    rescue e => error
      puts "Crawler Valid Date: Rescue #{e}"
      return false
    end
  end
end
