require 'net/http'
require 'proxy_fetcher'
require 'awesome_print'
require 'uri'
require 'nokogiri'
require 'open-uri'
require 'mechanize'

body = ProxyFetcher::Client.get 'http://mds.gov.br/area-de-imprensa/noticias'
document = Nokogiri::HTML(body)
document.css('div.tile-collective-nitf-content').each do |item|
  response = {
    source: url,
    url: item.css('div.tileContent a')[0].attributes["href"].value.gsub(' ',''),
    title: item.css('h2.tileHeadline').text.strip,
    subtitle: item.css('span.subtitle').text,
    publish_data: item.css('span.summary-view-icon')[0].text.strip,
    collection_date: Date.today,
    body_text: item.css('span.description').text
  }
  puts response
end

