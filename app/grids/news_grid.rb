class NewsGrid < BaseGrid

  scope do
    News
  end

  filter(:id, :integer, header: 'ID')
  filter(:source, :string, header: 'Fonte')
  filter(:url, :string, header: 'Link')
  filter(:title, :string, header: 'Titulo')
  filter(:subtitle, :string, header: 'SubTitulo')
  filter(:publish_data, :string, header: 'Data Publicacao')
  filter(:collection_date, :string, header: 'Data Coleta')
  filter(:body_text, :string, header: 'Texto')
  filter(:created_at, :date, :range => true)

  column(:id)
  column(:source)
  column(:url)
  column(:title)
  column(:subtitle)
  column(:publish_data)
  column(:collection_date)
  column(:body_text)
  date_column(:created_at)
end
