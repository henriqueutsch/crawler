# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
require 'faker'

News.destroy_all
10.times {
  News.create(
    source: 'http://www.source.com.br',
    url: 'http://www.url.com.br',
    title: Faker::Music.band,
    subtitle: Faker::Music.genre,
    publish_data: Date.today-rand(10000),
    collection_date: Date.today,
    body_text: Faker::Music.album
  )
}
