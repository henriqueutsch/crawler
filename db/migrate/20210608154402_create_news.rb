class CreateNews < ActiveRecord::Migration[5.2]
  def change
    create_table :news do |t|
      t.string :source
      t.string :url
      t.string :title
      t.string :subtitle
      t.date :publish_data
      t.date :collection_date
      t.string :body_text

      t.timestamps
    end
  end
end
