require 'open-uri'

namespace :run_crawler do
  desc 'CLEAN DATABASE'
  task cleandb: :environment do
    News.destroy_all
    Sidekiq.redis { |c| c.del('stat:processed') }
    News.reset_pk_sequence
  end

  desc 'POPULATE CRAWLER WITH MDS WEBSITE'
  task mds: :environment do
    puts 'Pegando total de paginas ...'
    document = Nokogiri::HTML(open('http://mds.gov.br/area-de-imprensa/noticias'))
    total_pages = document.css('a.pagina').text.to_i
    puts "Total Paginas MDS => #{total_pages}"
    (0..total_pages).map do |page|
      puts "Executando pagina => #{page}"
      CrawlerpageJob.perform_later("http://mds.gov.br/area-de-imprensa/noticias?b_start:int=#{page*12}")
    end
  end

  desc 'POPULATE CRAWLER WITH TUR WEBSITE'
  task tur: :environment do
    document = Nokogiri::HTML(open('http://mds.gov.br/area-de-imprensa/noticias'))
    total_pages = document.css('a.pagina').text.to_i
    (0..total_pages).map do |page|
      CrawlerpageJob.perform_later("http://mds.gov.br/area-de-imprensa/noticias?b_start:int=#{page*12}")
    end
  end
end
