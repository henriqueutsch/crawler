# README

## DONE
- [V] Rails docker-compose
- [V] Crawler 2

## TODO
- [] Regra Rodar com data anterior e Range
- [] Testes
# References

https://qiita.com/shimadama/items/3dee21f179b8100d3209

`Redis#exists(key)` will return an Integer in redis-rb 4.3. `exists?` returns a boolean, you should use it instead. To opt-in to the new behavior now you can set Redis.exists_returns_integer =  true. To disable this message and keep the current (boolean) behaviour of 'exists' you can set `Redis.exists_returns_integer = false`, but this option will be removed in 5.0. (/home/henriqueutsch/.asdf/installs/ruby/2.6.7/lib/ruby/gems/2.6.0/gems/sidekiq-6.0.7/lib/sidekiq/launcher.rb:160:in `block (2 levels) in ❤')

https://stackoverflow.com/questions/8059332/could-not-find-a-javascript-runtime-see-https-github-com-sstephenson-execjs-f

https://github.com/titusfortner/webdrivers
webdrivers downloads drivers and directs Selenium to use them. Currently supports:

Reset Sidekiq Count
Sidekiq.redis {|c| c.del('stat:processed') }

Reset ID
News.reset_pk_sequence

https://smartlogic.io/blog/how-to-test-a-sidekiq-worker/#:~:text=To%20test%20your%20Sidekiq%20Worker,be%20run%20as%20a%20job.
https://blog.appsignal.com/2019/10/29/sidekiq-optimization-and-monitoring.html
https://medium.com/rd-shipit/aumentando-o-desempenho-de-workers-do-sidekiq-com-go-57ac76e9c48c
https://github.com/digitalocean/go-workers2
https://www.reddit.com/r/rails/comments/9db946/can_we_scale_sidekiq_with_containers/
https://stackoverflow.com/questions/61862399/how-to-run-sidekiq-in-a-separate-different-docker-container-apart-from-applicati
https://www.loggly.com/blog/benchmarking-5-popular-load-balancers-nginx-haproxy-envoy-traefik-and-alb/
https://ichi.pro/pt/escalonando-sidekiq-workers-no-kubernetes-com-keda-111144170327540
https://kubernetes.io/docs/tasks/configure-pod-container/translate-compose-kubernetes/
https://www.cin.ufpe.br/~tg/2016-2/acaf.pdf
https://towardsdatascience.com/deploy-a-nodejs-microservices-to-a-docker-swarm-cluster-docker-from-zero-to-hero-464fa1369ea0
https://stackoverflow.com/questions/25845538/how-to-use-sudo-inside-a-docker-container
https://linoxide.com/give-normal-user-root-privileges/

curl http://localhost:3000/sidekiq/api/statistic/Worker.json\?dateFrom\=2015-07-30\&dateTo\=2020-06-08
https://github.com/sporkmonger/addressable
https://stackoverflow.com/questions/1805761/how-to-check-if-a-url-is-valid


https://opensource.com/article/18/5/how-find-ip-address-linux
require 'net/http'
https://opensource.com/article/18/6/vpn-alternatives
https://stackoverflow.com/questions/28852057/change-ip-address-dynamically
https://gimmeproxy.com/#how
https://free-proxy-list.net
https://www.bestproxyreviews.com/free-proxy-pool/
Net::HTTP.get(URI('http://ident.me'))
https://stackoverflow.com/questions/23383001/change-public-ip-address-of-ec2-instance-without-stop-start-or-elastic-ip
VPN AWS Project
- User Tor
- Use AWS
- Use external IP Proxy
- Use Private VPN

https://api.getproxylist.com/proxy
https://github.com/fgksgf/IP-Proxy-Pool


https://api.getproxylist.com/proxy?protocol%5B%5D=http

https://www.bestproxyreviews.com/free-proxy-pool/
https://github.com/nbulaj/proxy_fetcher

## net/http
https://ruby-doc.org/stdlib-2.4.2/libdoc/net/http/rdoc/Net/HTTP.html
https://stackoverflow.com/questions/15792999/how-to-set-a-proxy-in-rubys-net-http
Net::HTTP.new('example.com', nil, proxy_addr, proxy_port).start { |http|
  # always proxy via your.proxy.addr:8080
}
## mechanize
https://stackoverflow.com/questions/18348673/how-do-i-configure-a-ruby-mechanize-agent-to-work-through-the-charles-web-proxy
@agent ||= Mechanize.new do |agent|
  agent.set_proxy("localhost", 8888)
end
## watir
http://watir.com/guides/proxies/
proxy = 'my.proxy.com:8080'
browser = Watir::Browser.new :chrome, url: REMOTE_URL, proxy: {http: proxy, ssl: proxy}
## nokogiri

https://stackoverflow.com/questions/21752838/how-to-scrape-a-website-with-the-socksify-gem-proxy
https://github.com/freedomben/dory
https://github.com/sebyx07/active_proxy

https://scraperbox.com/blog/web-scraping-with-ruby
